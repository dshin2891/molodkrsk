package ru.molodkrsk.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.text.Html;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import ru.molodkrsk.R;
import ru.molodkrsk.models.FlagmanProgram;
import ru.molodkrsk.utils.ApiFactory;
import ru.molodkrsk.utils.FlagmanProgramID;
import ru.molodkrsk.utils.MolodkrskApi;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link KVNFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link KVNFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class KVNFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private static final int WEB_VIEW_TEXT_ZOOM = 10;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    @Bind(R.id.tvFlagmanTitle) protected TextView tvFlagmanTitle;
    @Bind(R.id.wvFlagmanContent) protected WebView wvFlagmanContent;
    @Bind(R.id.tvNoData) protected TextView tvNoData;
    @Bind(R.id.ivFlagmanImage) protected ImageView ivFlagmanImage;

    private static View view;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment KVNFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static KVNFragment newInstance(String param1, String param2) {
        KVNFragment fragment = new KVNFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public KVNFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.fragment_kvn, container, false);
        } catch (InflateException e) {
        /* map is already there, just return view as it is */
        }

        ButterKnife.bind(KVNFragment.this, view);

        ivFlagmanImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://vk.com/kraskvn24"));
                startActivity(intent);
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getFlagmanProgram();
    }

    private void getFlagmanProgram(){
//        MolodkrskApi molodkrskApi = ApiFactory.getApi();
//        Call<FlagmanProgram> call = molodkrskApi.flagmanProgram(FlagmanProgramID.KVN_ID.getValue());
//        //async request
//        Callback<FlagmanProgram> callback = new Callback<FlagmanProgram>() {
//            @Override
//            public void onResponse(Response<FlagmanProgram> response) {
//                if (response.isSuccess()) {
//                    FlagmanProgram flagmanProgram = response.body();
//                    tvFlagmanTitle.setText(stripHtml(flagmanProgram.getTitle()));
//                    wvFlagmanContent.loadDataWithBaseURL(MolodkrskApi.API_URL, flagmanProgram.getTxt(), "text/html", "UTF-8", "");
//                    tvNoData.setVisibility(View.GONE);
//                    wvFlagmanContent.setBackgroundColor(getResources().getColor(R.color.gray));
//                }
//            }
//
//            @Override
//            public void onFailure(Throwable t) {
//                tvNoData.setVisibility(View.VISIBLE);
//            }
//        };
//        call.enqueue(callback);

        tvFlagmanTitle.setText(getString(R.string.kvnFragment_title));
        WebSettings webSettings = wvFlagmanContent.getSettings();
        webSettings.setTextZoom(webSettings.getTextZoom() + WEB_VIEW_TEXT_ZOOM);
        wvFlagmanContent.loadDataWithBaseURL("", getString(R.string.kvn_txt), "text/html", "UTF-8", "");
        wvFlagmanContent.setBackgroundColor(getResources().getColor(R.color.gray));
        tvNoData.setVisibility(View.GONE);
    }

    private String stripHtml(String html) {
        return Html.fromHtml(html).toString();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

//    @Override
//    public void onAttach(Activity activity) {
//        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
