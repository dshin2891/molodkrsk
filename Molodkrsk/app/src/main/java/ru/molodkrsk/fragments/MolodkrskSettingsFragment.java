package ru.molodkrsk.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.MultiSelectListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.widget.Toast;

import com.squareup.okhttp.ResponseBody;

import java.util.Set;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import ru.molodkrsk.R;
import ru.molodkrsk.app.MolodkrskApplication;
import ru.molodkrsk.utils.ApiFactory;
import ru.molodkrsk.utils.MolodkrskApi;

public class MolodkrskSettingsFragment extends PreferenceFragment {

    MultiSelectListPreference menuFlagmanPrograms;
    private static String REGISTRATION_GCM_OK = "OK";
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);

        menuFlagmanPrograms = (MultiSelectListPreference)findPreference("menuFlagmanPrograms");

        menuFlagmanPrograms.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String flagmans = newValue.toString();
                if (!flagmans.equals("") || !flagmans.equals("[]")){
                    String fragmentIdList = flagmans.toString().replaceAll("[\\[\\]\\s]","");

                    MolodkrskApi molodkrskApi = ApiFactory.getApi();
                    Call<ResponseBody> call = molodkrskApi.sendFlagmansId(getRegistrationId(getActivity()),fragmentIdList);
                    //async request
                    Callback<ResponseBody> callback = new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Response<ResponseBody> response) {
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            Toast.makeText(getActivity(), getString(R.string.error_sendServer), Toast.LENGTH_LONG).show();
                        }
                    };
                    call.enqueue(callback);

                }
                return true;
            }
        });
    }

    private SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getActivity().getSharedPreferences(MolodkrskApplication.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }

    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            return "";
        }
        return registrationId;
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }
}
