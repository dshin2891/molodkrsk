package ru.molodkrsk.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import ru.molodkrsk.R;
import ru.molodkrsk.adapters.PreviewListAdapter;
import ru.molodkrsk.models.Preview;
import ru.molodkrsk.utils.ApiFactory;
import ru.molodkrsk.utils.MolodkrskApi;
import ru.molodkrsk.views.PreviewDetailsActivity;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PreviewsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PreviewsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PreviewsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    @Bind(R.id.lvPreviews) protected ListView lvPreviews;
    @Bind(R.id.tvEmptyView) protected TextView tvEmptyView;
    @Bind(R.id.refresh) protected SwipeRefreshLayout refresh;

    private static View view;
    private FragmentActivity contextActivity;


    private OnFragmentInteractionListener mListener;

    private PreviewListAdapter previewListAdapter;

    public static PreviewsFragment newInstance(String param1, String param2) {
        PreviewsFragment fragment = new PreviewsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public PreviewsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.fragment_previews, container, false);
        } catch (InflateException e) {
        /* map is already there, just return view as it is */
        }

        ButterKnife.bind(PreviewsFragment.this, view);

        lvPreviews.setEmptyView(tvEmptyView);

        refresh.setOnRefreshListener(this);
        refresh.setColorSchemeColors(getResources().getColor(R.color.orange_pressed));

        lvPreviews.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int topRowVerticalPosition = (lvPreviews == null || lvPreviews.getChildCount() == 0) ? 0 : lvPreviews.getChildAt(0).getTop();
                refresh.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
            }
        });

        lvPreviews.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intentPreviewDetails = new Intent(contextActivity, PreviewDetailsActivity.class);
                intentPreviewDetails.putExtra("previewText", previewListAdapter.getItem(position).getTxt());
                intentPreviewDetails.putExtra("previewImageId", previewListAdapter.getItem(position).getImg());
                intentPreviewDetails.putExtra("previewUrl", previewListAdapter.getItem(position).getUrl());
                intentPreviewDetails.putExtra("previewDate", previewListAdapter.getItem(position).getDatetime());
                intentPreviewDetails.putExtra("previewId", previewListAdapter.getItem(position).getId());
                intentPreviewDetails.putExtra("previewFlagmanId", previewListAdapter.getItem(position).getFlagman_id());
                startActivity(intentPreviewDetails);
            }
        });

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        contextActivity = (FragmentActivity)activity;

    }

    private void getPreviewList(){
        MolodkrskApi molodkrskApi = ApiFactory.getApi();
        Call<List<Preview>> call = molodkrskApi.previewList();
        //async request
        Callback<List<Preview>> callback = new Callback<List<Preview>>() {
            @Override
            public void onResponse(Response<List<Preview>> response) {
                if (response.isSuccess()) {
                    List<Preview> previewList = response.body();
                    previewListAdapter = new PreviewListAdapter(contextActivity,sortedPreviewList(previewList));
                    lvPreviews.setAdapter(previewListAdapter);
                    refresh.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                refresh.setRefreshing(false);
            }
        };
        call.enqueue(callback);
    }

    private List<Preview> sortedPreviewList(List<Preview> previewList){
        List<String> previewIdList = new ArrayList<>();
        List<Preview> previewSortedList = new ArrayList<>();
        SharedPreferences prefs = contextActivity.getSharedPreferences("ru.molodkrsk.deleted.preview", Context.MODE_PRIVATE);
        Type type = new TypeToken<List<String>>(){}.getType();
        if (prefs.getString("deletedPreviewList",null) != null) {
            previewIdList = new Gson().fromJson(prefs.getString("deletedPreviewList", null), type);

            for (int j = 0; j < previewList.size(); j ++){
                if (!previewIdList.contains(Integer.toString(previewList.get(j).getId())))
                    previewSortedList.add(previewList.get(j));
            }

            return previewSortedList;
        }else
            return previewList;
    }


    @Override
    public void onRefresh() {
        refresh.setRefreshing(true);
        getPreviewList();

    }

    @Override
    public void onResume() {
        super.onResume();
        getPreviewList();
    }

//    @Override
//    public void onAttach(Activity activity) {
//        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
