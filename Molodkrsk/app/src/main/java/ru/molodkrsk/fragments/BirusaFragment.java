package ru.molodkrsk.fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;

import butterknife.Bind;
import butterknife.ButterKnife;
import ru.molodkrsk.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class BirusaFragment extends Fragment {

    private static final int WEB_VIEW_TEXT_ZOOM = 10;

    @Bind(R.id.wvFlagmanContent) protected WebView wvFlagmanContent;
    @Bind(R.id.ivFlagmanImage) protected ImageView ivFlagmanImage;

    private static View view;

    public BirusaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.fragment_birusa, container, false);
        } catch (InflateException e) {
        /* map is already there, just return view as it is */
        }

        ButterKnife.bind(BirusaFragment.this, view);

        ivFlagmanImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://vk.com/biryusa_tim"));
                startActivity(intent);
            }
        });

        WebSettings webSettings = wvFlagmanContent.getSettings();
        webSettings.setTextZoom(webSettings.getTextZoom() + WEB_VIEW_TEXT_ZOOM);
        wvFlagmanContent.loadDataWithBaseURL("", getString(R.string.birusa_txt), "text/html", "UTF-8", "");
        wvFlagmanContent.setBackgroundColor(getResources().getColor(R.color.gray));

        return view;
    }
}
