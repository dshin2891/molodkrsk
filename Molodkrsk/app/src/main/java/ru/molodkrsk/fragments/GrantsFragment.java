package ru.molodkrsk.fragments;


import android.app.Fragment;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import butterknife.Bind;
import butterknife.ButterKnife;
import ru.molodkrsk.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class GrantsFragment extends Fragment {

    private static final int WEB_VIEW_TEXT_ZOOM = 10;

    @Bind(R.id.wvFlagmanContent) protected WebView wvFlagmanContent;

    private static View view;

    public GrantsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.fragment_grants, container, false);
        } catch (InflateException e) {
        /* map is already there, just return view as it is */
        }

        ButterKnife.bind(GrantsFragment.this, view);

        WebSettings webSettings = wvFlagmanContent.getSettings();
        webSettings.setTextZoom(webSettings.getTextZoom() + WEB_VIEW_TEXT_ZOOM);
        wvFlagmanContent.loadDataWithBaseURL("", getString(R.string.grants_txt), "text/html", "UTF-8", "");
        wvFlagmanContent.setBackgroundColor(getResources().getColor(R.color.gray));

        return view;
    }
}
