package ru.molodkrsk.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.gorbin.asne.core.SocialNetwork;
import com.github.gorbin.asne.core.SocialNetworkManager;
import com.github.gorbin.asne.core.listener.OnLoginCompleteListener;
import com.github.gorbin.asne.core.listener.OnPostingCompleteListener;
import com.github.gorbin.asne.facebook.FacebookSocialNetwork;
import com.github.gorbin.asne.twitter.TwitterSocialNetwork;
import com.github.gorbin.asne.vk.VkSocialNetwork;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;
import com.vk.sdk.VKScope;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import ru.molodkrsk.R;
import ru.molodkrsk.views.PreviewDetailsActivity;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PreviewDetailsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PreviewDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PreviewDetailsFragment extends Fragment implements SocialNetworkManager.OnInitializationCompleteListener, OnLoginCompleteListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private static final String SHARED_BASE_URL = "http://molodkrsk.ru/anonses/";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    @Bind(R.id.ivPreviewImage) protected ImageView ivPreviewImage;
    @Bind(R.id.description) protected TextView description;
    @Bind(R.id.fabShared) protected FloatingActionButton fabShared;
    @Bind(R.id.btnAdd) protected Button btnAdd;
    @Bind(R.id.btnDelete) protected Button btnDelete;
    @Bind(R.id.tvDate) protected TextView tvDate;
    @Bind(R.id.tvFlagmanProgram) TextView  tvFlagmanProgram;
    @Bind(R.id.ivFlagmanProgram) ImageView ivFlagmanProgram;


    private SimpleDateFormat formatLocale = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private SimpleDateFormat formatPreviewDate = new SimpleDateFormat("dd.MM.yyyy");

    private Toolbar toolbar;
    private AlertDialog sharedDialog;
    public static SocialNetworkManager mSocialNetworkManager;

    public String previewText;
    public String previewImageId;
    public String previewUrl;
    public String previewDate;
    public String previewFlagmanId;
    public int previewId;

    private static View view;

    private OnFragmentInteractionListener mListener;

    // TODO: Rename and change types and number of parameters
    public static PreviewDetailsFragment newInstance(String param1, String param2) {
        PreviewDetailsFragment fragment = new PreviewDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public PreviewDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.fragment_priview_details, container, false);
        } catch (InflateException e) {
        /* map is already there, just return view as it is */
        }

        ButterKnife.bind(PreviewDetailsFragment.this, view);

        toolbar = (Toolbar)view.findViewById(R.id.toolbar);
        toolbar.setTitle("");
        ((AppCompatActivity) PreviewDetailsFragment.this.getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) PreviewDetailsFragment.this.getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) view.findViewById(R.id.collapsing_toolbar);

        previewText = getActivity().getIntent().getStringExtra("previewText");
        previewImageId = getActivity().getIntent().getStringExtra("previewImageId");
        previewUrl = getActivity().getIntent().getStringExtra("previewUrl");
        previewDate = getActivity().getIntent().getStringExtra("previewDate");
        previewId = getActivity().getIntent().getIntExtra("previewId",-1);
        previewFlagmanId = getActivity().getIntent().getStringExtra("previewFlagmanId");

        String VK_KEY = getActivity().getString(R.string.vk_app_id);
        String TWITTER_CONSUMER_KEY = getActivity().getString(R.string.twitter_consumer_key);
        String TWITTER_CONSUMER_SECRET = getActivity().getString(R.string.twitter_consumer_secret);
        String TWITTER_CALLBACK_URL = "oauth://ASNE";

        String[] vkScope = new String[] {
                VKScope.WALL,
                VKScope.NOHTTPS,
        };

        ArrayList<String> fbScope = new ArrayList<String>();
        fbScope.addAll(Arrays.asList("public_profile, email, user_friends"));

        mSocialNetworkManager = (SocialNetworkManager) getFragmentManager().findFragmentByTag(PreviewDetailsActivity.SOCIAL_NETWORK_TAG);

        //Check if manager exist
        if (mSocialNetworkManager == null) {
            mSocialNetworkManager = new SocialNetworkManager();

            //Init and add to manager VkSocialNetwork
            VkSocialNetwork vkNetwork = new VkSocialNetwork(this, VK_KEY, vkScope);
            mSocialNetworkManager.addSocialNetwork(vkNetwork);

            //Init and add to manager FacebookSocialNetwork
            FacebookSocialNetwork fbNetwork = new FacebookSocialNetwork(this, fbScope);
            mSocialNetworkManager.addSocialNetwork(fbNetwork);

            //Init and add to manager TwitterSocialNetwork
            TwitterSocialNetwork twNetwork = new TwitterSocialNetwork(this, TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET,TWITTER_CALLBACK_URL);
            mSocialNetworkManager.addSocialNetwork(twNetwork);

            //Initiate every network from mSocialNetworkManager
            getFragmentManager().beginTransaction().add(mSocialNetworkManager, PreviewDetailsActivity.SOCIAL_NETWORK_TAG).commit();
            mSocialNetworkManager.setOnInitializationCompleteListener(this);
        } else {
            //if manager exist - get and setup login only for initialized SocialNetworks
            if(!mSocialNetworkManager.getInitializedSocialNetworks().isEmpty()) {
                List<SocialNetwork> socialNetworks = mSocialNetworkManager.getInitializedSocialNetworks();
                for (SocialNetwork socialNetwork : socialNetworks) {
                    socialNetwork.setOnLoginCompleteListener(this);
                    initSocialNetwork(socialNetwork);
                }
            }
        }

        //  collapsingToolbar.setTitle(getResources().getString(R.string.previewsFragment_title));

        setFlagmanProgram(Integer.parseInt(previewFlagmanId));
        
        try {
            tvDate.setText(formatPreviewDate.format(formatLocale.parse(previewDate)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        description.setText(stripHtml(previewText));

        Picasso.with(PreviewDetailsFragment.this.getActivity())
                .load(String.format(Locale.US, "http://molodkrsk.ru/upload/articles_events/images/s_%s", previewImageId))
                .fit()
                .centerCrop()
                .into(ivPreviewImage);

        fabShared.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedDialog().show();
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(PreviewDetailsFragment.this.getActivity())
                        .setTitle(R.string.app_name)
                        .setMessage(R.string.addCalendar_massage)
                        .setPositiveButton(R.string.dialog_yes_button, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                addEventToCalendar();
                            }
                        })
                        .setNegativeButton(R.string.dialog_no_button, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .setIcon(R.drawable.ic_launcher)
                        .show();
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(PreviewDetailsFragment.this.getActivity())
                        .setTitle(R.string.app_name)
                        .setMessage(R.string.delete_preview_massage)
                        .setPositiveButton(R.string.dialog_yes_button, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                List<String> previewIdList = new ArrayList<String>();

                                SharedPreferences prefs = getActivity().getSharedPreferences("ru.molodkrsk.deleted.preview", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = prefs.edit();

                                Type type = new TypeToken<List<String>>(){}.getType();
                                if (prefs.getString("deletedPreviewList",null) != null)
                                    previewIdList = new Gson().fromJson(prefs.getString("deletedPreviewList",null), type);

                                previewIdList.add(Integer.toString(previewId));

                                editor.putString("deletedPreviewList",new Gson().toJson(previewIdList));
                                editor.commit();
                                dialog.dismiss();
                                getActivity().finish();

                            }
                        })
                        .setNegativeButton(R.string.dialog_no_button, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .setIcon(R.drawable.ic_launcher)
                        .show();

            }
        });

        return view;
    }

    public void setFlagmanProgram(int flagmanID) {
        switch (flagmanID) {
            case 0:
                break;
            case 87:
                setFlagmanProgramData(getString(R.string.artParadeFragment_title),R.drawable.art);
                break;
            case 88:
                setFlagmanProgramData(getString(R.string.associationOfStudentsSportsFragment_title), R.drawable.sport);
                break;
            case 89:
                setFlagmanProgramData(getString(R.string.runWithMeSiberianFragment_title), R.drawable.runwithme);
                break;
            case 90:
                setFlagmanProgramData(getString(R.string.volunteerismFragment_title), R.drawable.heart);
                break;
            case 91:
                setFlagmanProgramData(getString(R.string.historicalMemoryFragment_title), R.drawable.memory);
                break;
            case 92:
                setFlagmanProgramData(getString(R.string.kvnFragment_title), R.drawable.kvn);
                break;
            case 93:
                setFlagmanProgramData(getString(R.string.kksoFragment_title), R.drawable.kkso);
                break;
            case 95:
                setFlagmanProgramData(getString(R.string.teamFragment_title), R.drawable.team);
                break;
            case 96:
                setFlagmanProgramData(getString(R.string.roboticsFragment_title), R.drawable.robot);
                break;
            case 100:
                setFlagmanProgramData(getString(R.string.myTerritoryFragment_title), R.drawable.terr);
                break;
            case 98:
                setFlagmanProgramData(getString(R.string.youBusinessManFragment_title), R.drawable.businessman);
                break;
            case 99:
                setFlagmanProgramData(getString(R.string.extremeSportFragment_title), R.drawable.extreme);
                break;
            case 24:
                setFlagmanProgramData(getString(R.string.kamenkaFragment_title),R.drawable.kamenka);
                break;
            case 16:
                setFlagmanProgramData(getString(R.string.juniorFragment_title),R.drawable.unior);
                break;
            case 17:
                setFlagmanProgramData(getString(R.string.birusaFragment_title),R.drawable.birusa);
                break;
            default:
                break;
        }
    }

    private void setFlagmanProgramData(String flagmanProgramTitle, int flagmanProgramImageId){
        tvFlagmanProgram.setText(flagmanProgramTitle);
        ivFlagmanProgram.setImageResource(flagmanProgramImageId);
    }

    private void addEventToCalendar(){
        long startTime = 0;
        try {
             startTime = formatLocale.parse(previewDate).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (isEventInCalendar(startTime,"test")){
            new AlertDialog.Builder(PreviewDetailsFragment.this.getActivity())
                    .setTitle(R.string.app_name)
                    .setMessage(R.string.addCalendar_error)
                    .setPositiveButton(R.string.dialog_ok_button, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                           dialog.dismiss();
                        }
                    })
                    .setIcon(R.drawable.ic_launcher)
                    .show();
        }else {
            Intent intent = new Intent(Intent.ACTION_EDIT);
            intent.setType("vnd.android.cursor.item/event");
            intent.putExtra("beginTime", startTime);
            intent.putExtra("allDay", true);
            intent.putExtra("endTime", startTime+60*60*1000);
            intent.putExtra("title", "test");
            startActivity(intent);
        }
    }

    private boolean isEventInCalendar(long startTime, String title){
                String[] proj =
                new String[]{
                        CalendarContract.Instances._ID,
                        CalendarContract.Instances.TITLE,
                        CalendarContract.Instances.BEGIN,
                        CalendarContract.Instances.END};
        Cursor cursor =
                CalendarContract.Instances.query(PreviewDetailsFragment.this.getActivity().getContentResolver(), proj, startTime, startTime + 60 * 60 * 1000);
        if (cursor.getCount() > 0) {
//            cursor.moveToFirst();
            while (cursor.moveToNext()){
                if (cursor.getString(1).equals(title))
                    return true;
                else
                    return false;
            }
            return false;
        }else
            return false;

    }

    private void initSocialNetwork(SocialNetwork socialNetwork){
        if(socialNetwork.isConnected()){
            switch (socialNetwork.getID()){
                case VkSocialNetwork.ID:
                    break;
            }
        }
    }
    @Override
    public void onSocialNetworkManagerInitialized() {
        //when init SocialNetworks - get and setup login only for initialized SocialNetworks
        for (SocialNetwork socialNetwork : mSocialNetworkManager.getInitializedSocialNetworks()) {
            socialNetwork.setOnLoginCompleteListener(this);
            initSocialNetwork(socialNetwork);
        }
    }

    private View.OnClickListener shareClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int networkId = 0;
            switch (view.getId()){
                case R.id.ivVkontakte:
                    networkId = VkSocialNetwork.ID;
                    break;
                case R.id.ivFacebook:
                    networkId = FacebookSocialNetwork.ID;
                    break;
                case R.id.ivTwitter:
                    networkId = TwitterSocialNetwork.ID;
                    break;
                default:
                    break;
            }
            SocialNetwork socialNetwork = mSocialNetworkManager.getSocialNetwork(networkId);
            if(!socialNetwork.isConnected()) {
                if(networkId != 0) {
                    socialNetwork.requestLogin();
                    PreviewDetailsActivity.showProgress("Loading social person");
                } else {
                    Toast.makeText(getActivity(), "Wrong networkId", Toast.LENGTH_LONG).show();
                }
            } else {
                sharePreview(socialNetwork.getID());
            }
        }
    };

    @Override
    public void onLoginSuccess(int networkId) {
        PreviewDetailsActivity.hideProgress();
        sharePreview(networkId);
    }

    @Override
    public void onError(int networkId, String requestID, String errorMessage, Object data) {
        PreviewDetailsActivity.hideProgress();
        Toast.makeText(getActivity(), getString(R.string.login_error), Toast.LENGTH_LONG).show();
    }

    private void sharePreview(int networkId){
        Bundle postParams = new Bundle();
        postParams.putString(SocialNetwork.BUNDLE_LINK, SHARED_BASE_URL+previewUrl);
        mSocialNetworkManager.getSocialNetwork(networkId).requestPostLink(postParams, "", postingComplete);
    }

    private OnPostingCompleteListener postingComplete = new OnPostingCompleteListener() {
        @Override
        public void onPostSuccessfully(int socialNetworkID) {
            Toast.makeText(getActivity(), getString(R.string.post_success), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onError(int socialNetworkID, String requestID, String errorMessage, Object data) {
            Toast.makeText(getActivity(), getString(R.string.post_error), Toast.LENGTH_LONG).show();
        }
    };

    private String stripHtml(String html) {
        return Html.fromHtml(html).toString();
    }

    private Dialog sharedDialog(){
        AlertDialog.Builder adb = new AlertDialog.Builder(PreviewDetailsFragment.this.getActivity());
        adb.setNegativeButton(getString(R.string.dialog_cancel_button), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                sharedDialog.dismiss();
            }
        });
        sharedDialog = adb.create();
        sharedDialog.setTitle(R.string.dialog_shared_title);

        RelativeLayout view = (RelativeLayout) PreviewDetailsFragment.this.getActivity().getLayoutInflater()
                .inflate(R.layout.dialog_shared, null);

        ImageView ivVkontakte = (ImageView)view.findViewById(R.id.ivVkontakte);
        ImageView ivFacebook = (ImageView)view.findViewById(R.id.ivFacebook);
        ImageView ivTwitter = (ImageView)view.findViewById(R.id.ivTwitter);

        ivVkontakte.setOnClickListener(shareClick);
        ivFacebook.setOnClickListener(shareClick);
        ivTwitter.setOnClickListener(shareClick);

        sharedDialog.setView(view);
        return sharedDialog;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

//    @Override
//    public void onAttach(Activity activity) {
//        super.onAttach(activity);
//        try {
//            mListener = (OnFragmentInteractionListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }
}
