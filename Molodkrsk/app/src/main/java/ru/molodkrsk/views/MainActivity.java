package ru.molodkrsk.views;

import android.app.FragmentManager;
import android.content.res.Configuration;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;

import butterknife.Bind;
import butterknife.ButterKnife;
import ru.molodkrsk.R;
import ru.molodkrsk.adapters.NavigationDrawListAdapter;
import ru.molodkrsk.adapters.SectionNavigationDrawListAdapter;
import ru.molodkrsk.fragments.ArtParadeFragment;
import ru.molodkrsk.fragments.AssociationOfStudentsSportsFragment;
import ru.molodkrsk.fragments.BirusaFragment;
import ru.molodkrsk.fragments.ExtremeSportFragment;
import ru.molodkrsk.fragments.GrantsFragment;
import ru.molodkrsk.fragments.HistoricalMemoryFragment;
import ru.molodkrsk.fragments.JuniorFragment;
import ru.molodkrsk.fragments.KKSOFragment;
import ru.molodkrsk.fragments.KVNFragment;
import ru.molodkrsk.fragments.KamenkaFragment;
import ru.molodkrsk.fragments.MolodkrskSettingsFragment;
import ru.molodkrsk.fragments.MyTerritoryFragment;
import ru.molodkrsk.fragments.PreviewsFragment;
import ru.molodkrsk.fragments.RoboticsFragment;
import ru.molodkrsk.fragments.RunWithMeSiberianFragment;
import ru.molodkrsk.fragments.TeamFragment;
import ru.molodkrsk.fragments.VolunteerismFragment;
import ru.molodkrsk.fragments.YouBusinessManFragment;
import ru.molodkrsk.models.NavigationMenuLIstItem;

public class MainActivity extends AppCompatActivity implements ListView.OnItemClickListener{

    @Bind(R.id.navigation_drawer) protected DrawerLayout navigation_drawer;
    @Bind(R.id.lvNavigationItemList) protected ListView lvNavigationItemList;

    private Toolbar toolbar;
    private ActionBarDrawerToggle drawerToggle;
    private NavigationView navigation_view;
    private SectionNavigationDrawListAdapter sectionNavigationDrawListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        View footerView = getLayoutInflater().inflate(R.layout.lv_navigation_item_footer, null);

//        Intent intent = new Intent(this, PreviewDetailsActivity.class);
//        startActivity(intent);
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        navigation_view = (NavigationView)findViewById(R.id.navigation_view);
        setSupportActionBar(toolbar);

        lvNavigationItemList.addFooterView(footerView);

//        navigation_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
//            @Override
//            public boolean onNavigationItemSelected(MenuItem menuItem) {
//                menuItem.setChecked(true);
//                int menu_item_id = menuItem.getItemId();
//                setContentFragment(menu_item_id);
//                setToolbarTitle(menu_item_id);
//                navigation_drawer.closeDrawer(navigation_view);
//                return true;
//            }
//        });

        lvNavigationItemList.setOnItemClickListener(this);

        sectionNavigationDrawListAdapter = new SectionNavigationDrawListAdapter(this);
        NavigationDrawListAdapter navigationFlagmanDrawListAdapter;
        NavigationDrawListAdapter navigationPreviewDrawListAdapter;
        NavigationDrawListAdapter navigationProjectsDrawListAdapter;

        navigationPreviewDrawListAdapter = new NavigationDrawListAdapter(
                this,
                R.layout.row_navigation_drawer_list_item,
                new NavigationMenuLIstItem[]{
                        new NavigationMenuLIstItem(R.drawable.announcement, getString(R.string.previewsFragment_title))
                });

        navigationFlagmanDrawListAdapter = new NavigationDrawListAdapter(
                this,
                R.layout.row_navigation_drawer_list_item,
                new NavigationMenuLIstItem[]{
                        new NavigationMenuLIstItem(R.drawable.art, getString(R.string.artParadeFragment_title)),
                        new NavigationMenuLIstItem(R.drawable.sport, getString(R.string.associationOfStudentsSportsFragment_title)),
                        new NavigationMenuLIstItem(R.drawable.runwithme, getString(R.string.runWithMeSiberianFragment_title)),
                        new NavigationMenuLIstItem(R.drawable.heart, getString(R.string.volunteerismFragment_title)),
                        new NavigationMenuLIstItem(R.drawable.memory, getString(R.string.historicalMemoryFragment_title)),
                        new NavigationMenuLIstItem(R.drawable.kvn, getString(R.string.kvnFragment_title)),
                        new NavigationMenuLIstItem(R.drawable.kkso, getString(R.string.kksoFragment_title)),
                        new NavigationMenuLIstItem(R.drawable.team, getString(R.string.teamFragment_title)),
                        new NavigationMenuLIstItem(R.drawable.robot, getString(R.string.roboticsFragment_title)),
                        new NavigationMenuLIstItem(R.drawable.terr, getString(R.string.myTerritoryFragment_title)),
                        new NavigationMenuLIstItem(R.drawable.businessman, getString(R.string.youBusinessManFragment_title)),
                        new NavigationMenuLIstItem(R.drawable.extreme, getString(R.string.extremeSportFragment_title))
                });

        navigationProjectsDrawListAdapter = new NavigationDrawListAdapter(
                this,
                R.layout.row_navigation_drawer_list_item,
                new NavigationMenuLIstItem[]{
                        new NavigationMenuLIstItem(R.drawable.kamenka, getString(R.string.kamenkaFragment_title)),
                        new NavigationMenuLIstItem(R.drawable.birusa, getString(R.string.birusaFragment_title)),
                        new NavigationMenuLIstItem(R.drawable.unior, getString(R.string.juniorFragment_title)),
                        new NavigationMenuLIstItem(R.drawable.grants, getString(R.string.grantsFragment_title))
                });

        sectionNavigationDrawListAdapter.addSection("",navigationPreviewDrawListAdapter);
        sectionNavigationDrawListAdapter.addSection(getString(R.string.flagman_programs), navigationFlagmanDrawListAdapter);
        sectionNavigationDrawListAdapter.addSection(getString(R.string.projects), navigationProjectsDrawListAdapter);

        lvNavigationItemList.setAdapter(sectionNavigationDrawListAdapter);

        lvNavigationItemList.performItemClick(lvNavigationItemList, 1, lvNavigationItemList.getItemIdAtPosition(1));

        drawerToggle = new ActionBarDrawerToggle(this, navigation_drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

            }
        };

        footerView.setClickable(true);
        footerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame, new MolodkrskSettingsFragment())
                        .commit();

                setTitle(getString(R.string.settings));
                navigation_drawer.closeDrawer(navigation_view);
            }
        });

//        navigation_drawer.setDrawerListener(drawerToggle);
//
        setContentFragment(R.id.navigation_item_1);
        setToolbarTitle(R.id.navigation_item_1);
     //   setPreviewFragment();
    }

//    private void setPreviewFragment(){
//        FragmentManager fragmentManager = getFragmentManager();
//        fragmentManager.beginTransaction()
//                .replace(R.id.frame, new PreviewsFragment())
//                .commit();
//
//        setTitle(getString(R.string.previewsFragment_title));
//        navigation_drawer.closeDrawer(navigation_view);
//    }

    public void onItemClick(AdapterView parent, View view, int position, long id) {

        if (position == 1)
            position = position - 1;
        else
            position = position - 2;

        setContentFragment(position);
        lvNavigationItemList.setItemChecked(position, true);
        setToolbarTitle(position);
        navigation_drawer.closeDrawer(navigation_view);
    }

    private void setContentFragment(int menu_item_id){
        FragmentManager fragmentManager = getFragmentManager();
        switch (menu_item_id) {
            case 0:
                fragmentManager.beginTransaction()
                        .replace(R.id.frame, new PreviewsFragment())
                        .commit();
                break;
            case 1:
                fragmentManager.beginTransaction()
                        .replace(R.id.frame, new ArtParadeFragment())
                        .commit();
                break;
            case 2:
                fragmentManager.beginTransaction()
                        .replace(R.id.frame, new AssociationOfStudentsSportsFragment())
                        .commit();
                break;
            case 3:
                fragmentManager.beginTransaction()
                        .replace(R.id.frame, new RunWithMeSiberianFragment())
                        .commit();
                break;
            case 4:
                fragmentManager.beginTransaction()
                        .replace(R.id.frame, new VolunteerismFragment())
                        .commit();
                break;
            case 5:
                fragmentManager.beginTransaction()
                        .replace(R.id.frame, new HistoricalMemoryFragment())
                        .commit();
                break;
            case 6:
                fragmentManager.beginTransaction()
                        .replace(R.id.frame, new KVNFragment())
                        .commit();
                break;
            case 7:
                fragmentManager.beginTransaction()
                        .replace(R.id.frame, new KKSOFragment())
                        .commit();
                break;
            case 8:
                fragmentManager.beginTransaction()
                        .replace(R.id.frame, new TeamFragment())
                        .commit();
                break;
            case 9:
                fragmentManager.beginTransaction()
                        .replace(R.id.frame, new RoboticsFragment())
                        .commit();
                break;
            case 10:
                fragmentManager.beginTransaction()
                        .replace(R.id.frame, new MyTerritoryFragment())
                        .commit();
                break;
            case 11:
                fragmentManager.beginTransaction()
                        .replace(R.id.frame, new YouBusinessManFragment())
                        .commit();
                break;
            case 12:
                fragmentManager.beginTransaction()
                        .replace(R.id.frame, new ExtremeSportFragment())
                        .commit();
                break;
            case 14:
                fragmentManager.beginTransaction()
                        .replace(R.id.frame, new KamenkaFragment())
                        .commit();
                break;
            case 15:
                fragmentManager.beginTransaction()
                        .replace(R.id.frame, new BirusaFragment())
                        .commit();
                break;
            case 16:
                fragmentManager.beginTransaction()
                        .replace(R.id.frame, new JuniorFragment())
                        .commit();
                break;
            case 17:
                fragmentManager.beginTransaction()
                        .replace(R.id.frame, new GrantsFragment())
                        .commit();
                break;
            default:
                break;
        }
    }

    public void setToolbarTitle(int menu_item_id) {
        switch (menu_item_id) {
            case 0:
                setTitle(R.string.previewsFragment_title);
                break;
            case 1:
                setTitle(getString(R.string.artParadeFragment_title));
                break;
            case 2:
                setTitle(getString(R.string.associationOfStudentsSportsFragment_title));
                break;
            case 3:
                setTitle(getString(R.string.runWithMeSiberianFragment_title));
                break;
            case 4:
                setTitle(getString(R.string.volunteerismFragment_title));
                break;
            case 5:
                setTitle(getString(R.string.historicalMemoryFragment_title));
                break;
            case 6:
                setTitle(getString(R.string.kvnFragment_title));
                break;
            case 7:
                setTitle(getString(R.string.kksoFragment_title));
                break;
            case 8:
                setTitle(getString(R.string.teamFragment_title));
                break;
            case 9:
                setTitle(getString(R.string.roboticsFragment_title));
                break;
            case 10:
                setTitle(getString(R.string.myTerritoryFragment_title));
                break;
            case 11:
                setTitle(getString(R.string.youBusinessManFragment_title));
                break;
            case 12:
                setTitle(getString(R.string.extremeSportFragment_title));
                break;
            case 14:
                setTitle(getString(R.string.kamenkaFragment_title));
                break;
            case 15:
                setTitle(getString(R.string.birusaFragment_title));
                break;
            case 16:
                setTitle(getString(R.string.juniorFragment_title));
                break;
            case 17:
                setTitle(getString(R.string.grantsFragment_title));
                break;
            default:
                break;
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
