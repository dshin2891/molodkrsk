package ru.molodkrsk.utils;

import retrofit.Call;
import retrofit.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by Дмитрий on 15.09.2015.
 */
public interface ReportSenderApi {

    String API_URL = "http://v2.monitor-soft.ru/api";

    @FormUrlEncoded
    @POST("/log?appName=Molodkrsk")
    Call<String> sendReport(@Field("Raw") String raw);
}

