package ru.molodkrsk.utils;

import com.squareup.okhttp.ResponseBody;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;
import ru.molodkrsk.models.FlagmanProgram;
import ru.molodkrsk.models.Preview;

/**
 * Created by Дмитрий on 24.09.2015.
 */
public interface MolodkrskApi {

    String API_URL = "http://molodkrsk.ru";
    String BASE_URL_FOR_FLAGMAN_IMAGE = "http://molodkrsk.ru/upload/catalog_brands/images/s_";

    @GET("/api/get_news")
    Call<List<Preview>> previewList();

    @GET("/api/get_flagman")
    Call<FlagmanProgram> flagmanProgram(@Query("id") int id);

    @GET("/api/register_device")
    Call<ResponseBody> sendRegistrationId(@Query("device_type") int device_type,@Query("device_id") String device_id);

    @GET("/api/set_flagmans")
    Call<ResponseBody> sendFlagmansId(@Query("device_id") String device_id,@Query("flagmans") String flagmans);
}
