package ru.molodkrsk.utils;

import android.content.Context;
import android.util.Log;

import org.acra.ReportField;
import org.acra.collector.CrashReportData;
import org.acra.sender.ReportSender;
import org.acra.sender.ReportSenderException;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by Дмитрий on 21.08.2015.
 */
public class MolodkrskReportSender implements ReportSender {

    private static final String BASE_URL = "http://v2.monitor-soft.ru/api/log?appName=Molodkrsk";
    private static final String TAG = "myLogs";
    @Override
    public void send(Context context, CrashReportData report) throws ReportSenderException {
        String log = createCrashLog(report);
        String url = BASE_URL;
        Log.d(TAG, "Зашли в ReportSender");
//        try {
//            ReportSenderApi reportSenderApi = ApiFactory.getReportSenderApi();
//            Call<String> call = reportSenderApi.sendReport(log);
//            call.execute();
//        } catch (IOException e) {
//            e.printStackTrace();
//            Log.d(TAG, e.getMessage());
//        }
//        try {
//            ReportSenderApi reportSenderApi = ApiFactory.getReportSenderApi();
//            Call<String> call = reportSenderApi.sendReport(log);
//            //async request
//            Callback<String> callback = new Callback<String>() {
//                @Override
//                public void onResponse(Response<String> response) {
//                    if (response.isSuccess()) {
//                        String massage = response.message();
//                    }
//                }
//
//                @Override
//                public void onFailure(Throwable t) {
//                   String trdt = t.getMessage();
//                }
//            };
//            call.enqueue(callback);
//        } catch (Exception e) {
//            e.printStackTrace();
//            Log.d(TAG, e.getMessage());
//        }


        try {
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);

            List<NameValuePair> parameters = new ArrayList<NameValuePair>();
            parameters.add(new BasicNameValuePair("Raw", log));
            httpPost.setEntity(new UrlEncodedFormEntity(parameters, HTTP.UTF_8));
            httpClient.execute(httpPost);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String createCrashLog(CrashReportData report) {
        Date now = new Date();
        StringBuilder log = new StringBuilder();
        log.append("Package: ").append(report.get(ReportField.PACKAGE_NAME)).append("\n");
        log.append("Version: ").append(report.get(ReportField.APP_VERSION_CODE)).append("\n");
        log.append("Android: ").append(report.get(ReportField.ANDROID_VERSION)).append("\n");
        log.append("Manufacturer: ").append(android.os.Build.MANUFACTURER).append("\n");
        log.append("Model: ").append(report.get(ReportField.PHONE_MODEL)).append("\n");
        log.append("Date: ").append(now).append("\n");
        log.append("\n");
        log.append(report.get(ReportField.STACK_TRACE));

        return log.toString();
    }
}
