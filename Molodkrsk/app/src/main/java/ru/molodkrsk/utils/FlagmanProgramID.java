package ru.molodkrsk.utils;

/**
 * Created by Дмитрий on 28.09.2015.
 */
public enum  FlagmanProgramID {
    ART_PARADE_ID(87),
    ASSOCIATION_OF_STUDENTS_SPORTS_ID(88),
    RUNWITHME_SIBERIAN_ID(89),
    VOLUNTEERISM_ID(90),
    HISTORICAL_MEMORY_ID(91),
    KVN_ID(92),
    KKSO_ID(93),
    TEAM_ID(95),
    ROBOTICS_ID(96),
    MY_TERRITORY_ID(100),
    YOU_BUISNESSMAN_ID(98),
    EXTREME_SPORT_ID(99);
    private final int value;

    private FlagmanProgramID(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}
