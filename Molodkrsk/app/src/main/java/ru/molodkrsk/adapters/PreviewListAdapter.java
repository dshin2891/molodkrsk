package ru.molodkrsk.adapters;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import ru.molodkrsk.R;
import ru.molodkrsk.models.Preview;

/**
 * Created by Дмитрий on 24.09.2015.
 */
public class PreviewListAdapter extends ArrayAdapter<Preview> {

    private final Context context;
    private final List<Preview> previewList;
    private SimpleDateFormat formatLocale = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private SimpleDateFormat formatPreviewDate = new SimpleDateFormat("dd.MM.yyyy");

    public PreviewListAdapter(Context context,List<Preview> previewList){
        super(context, R.layout.row_preview_list,previewList);

        this.context = context;
        this.previewList = previewList;
    }

    @Override
    public int getCount() {
        return previewList.size();
    }

    Preview getPreview(int position) {
        return previewList.get(position);
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.row_preview_list, parent, false);
            holder = new ViewHolder();
            holder.title = (TextView) convertView.findViewById(R.id.tvPreviewText);
            holder.icon = (ImageView) convertView.findViewById(R.id.ivPreviewImage);
            holder.date = (TextView) convertView.findViewById(R.id.tvPreviewDate);
            holder.flagmanIcon = (ImageView)convertView.findViewById(R.id.ivFlagmanProgram);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        Preview preview = getPreview(position);
        holder.title.setText(stripHtml(preview.getTxt()));

        try {
            holder.date.setText(formatPreviewDate.format(formatLocale.parse(preview.getDatetime())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Picasso.with(context)
                .load(String.format(Locale.US, "http://molodkrsk.ru/upload/articles_events/images/s_%s", preview.getImg()))
                .fit()
                .into(holder.icon);

        setFlagmanProgram(Integer.parseInt(preview.getFlagman_id()),holder);

        return convertView;
    }

    private String stripHtml(String html) {
        return Html.fromHtml(html).toString();
    }

    private static class ViewHolder {

        public TextView title;
        public ImageView icon;
        public TextView date;
        public ImageView flagmanIcon;
    }

    private void setFlagmanProgramData(int flagmanProgramImageId,ViewHolder holder){
        holder.flagmanIcon.setImageResource(flagmanProgramImageId);
    }

    public void setFlagmanProgram(int flagmanID,ViewHolder holder) {
        switch (flagmanID) {
            case 0:
                break;
            case 87:
                setFlagmanProgramData(R.drawable.art,holder);
                break;
            case 88:
                setFlagmanProgramData(R.drawable.sport,holder);
                break;
            case 89:
                setFlagmanProgramData(R.drawable.runwithme,holder);
                break;
            case 90:
                setFlagmanProgramData(R.drawable.heart,holder);
                break;
            case 91:
                setFlagmanProgramData(R.drawable.memory,holder);
                break;
            case 92:
                setFlagmanProgramData(R.drawable.kvn,holder);
                break;
            case 93:
                setFlagmanProgramData(R.drawable.kkso,holder);
                break;
            case 95:
                setFlagmanProgramData(R.drawable.team,holder);
                break;
            case 96:
                setFlagmanProgramData(R.drawable.robot,holder);
                break;
            case 100:
                setFlagmanProgramData(R.drawable.terr,holder);
                break;
            case 98:
                setFlagmanProgramData(R.drawable.businessman,holder);
                break;
            case 99:
                setFlagmanProgramData(R.drawable.extreme,holder);
                break;
            case 24:
                setFlagmanProgramData(R.drawable.kamenka,holder);
                break;
            case 16:
                setFlagmanProgramData(R.drawable.unior,holder);
                break;
            case 17:
                setFlagmanProgramData(R.drawable.birusa,holder);
                break;
            default:
                break;
        }
    }
}
