package ru.molodkrsk.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import ru.molodkrsk.R;
import ru.molodkrsk.models.NavigationMenuLIstItem;

/**
 * Created by Дмитрий on 14.09.2015.
 */
public class NavigationDrawListAdapter extends ArrayAdapter<NavigationMenuLIstItem> {

    Context mContext;
    int layoutResourceId;
    NavigationMenuLIstItem data[] = null;

    public NavigationDrawListAdapter(Context mContext, int layoutResourceId, NavigationMenuLIstItem[] data) {
        super(mContext, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.length;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View listItem = convertView;

        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listItem = inflater.inflate(layoutResourceId, parent, false);


        ImageView imageViewIcon = (ImageView) listItem.findViewById(R.id.imageViewIcon);
        TextView textViewName = (TextView) listItem.findViewById(R.id.textViewName);

        NavigationMenuLIstItem navigationMenuLIstItem = data[position];

        imageViewIcon.setImageResource(navigationMenuLIstItem.icon);
        textViewName.setText(navigationMenuLIstItem.name);

        return listItem;
    }
}
