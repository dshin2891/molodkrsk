package ru.molodkrsk.app;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.squareup.okhttp.ResponseBody;

import org.acra.ACRA;
import org.acra.ReportField;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import ru.molodkrsk.R;
import ru.molodkrsk.utils.ApiFactory;
import ru.molodkrsk.utils.MolodkrskApi;
import ru.molodkrsk.utils.MolodkrskReportSender;

/**
 * Created by Дмитрий on 21.08.2015.
 */

@ReportsCrashes(
        customReportContent = { ReportField.APP_VERSION_CODE, ReportField.APP_VERSION_NAME, ReportField.ANDROID_VERSION, ReportField.PHONE_MODEL, ReportField.STACK_TRACE, ReportField.LOGCAT },
        mode = ReportingInteractionMode.TOAST,
        resToastText = R.string.error)
public class MolodkrskApplication extends MultiDexApplication {
    private static String REGISTRATION_GCM_OK = "OK";
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private static String SENDER_ID = "167800000711";
    private Intent broadcastIntent;
    private GoogleCloudMessaging gcm;
    private String regid;
    private Timer timer = new Timer();

    @Override
    public void onCreate() {
       // ErrorReporter.getInstance().setReportSender(new MolodkrskReportSender());
        super.onCreate();
        ACRA.init(this);
        ACRA.getErrorReporter().setReportSender(new MolodkrskReportSender());

        if (checkPlayServices()) {
            gcm = GoogleCloudMessaging.getInstance(this);
            regid = getRegistrationId(this);

            if (regid.isEmpty()) {
                // registerInBackground();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        new GCMRegistration().execute();
                    }
                }, 0L, 60L * 1000);
            }else {
                SharedPreferences prefs = getSharedPreferences("ru.molodkrsk", Context.MODE_PRIVATE);
                if (prefs.getBoolean("isSendRegId",false))
                    sendRegID();
            }
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(MolodkrskApplication.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }

    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            return "";
        }
        return registrationId;
    }

    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences(context);
        int appVersion = getAppVersion(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            return false;
        }
        return true;
    }

    class GCMRegistration extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... param)
        {
            if (gcm == null)
                gcm = GoogleCloudMessaging.getInstance(MolodkrskApplication.this);

            try {
                regid = gcm.register(SENDER_ID);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }

            storeRegistrationId(MolodkrskApplication.this, regid);
            return REGISTRATION_GCM_OK;
        }

        @Override
        protected void onPostExecute(String result){

            super.onPostExecute(result);

            if (result == null){
                return;
            }else if (result.equals(REGISTRATION_GCM_OK)){
                timer.cancel();
                timer.purge();
                sendRegID();
            }
        }
    }
    private void sendRegID(){
        MolodkrskApi molodkrskApi = ApiFactory.getApi();
        Call<ResponseBody> call = molodkrskApi.sendRegistrationId(1,getRegistrationId(MolodkrskApplication.this));
        SharedPreferences prefs = getSharedPreferences("ru.molodkrsk", Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = prefs.edit();
        //async request
        Callback<ResponseBody> callback = new Callback<ResponseBody>() {
            @Override
            public void onResponse(Response<ResponseBody> response) {

                editor.putBoolean("isSendRegId",false);
                editor.commit();
            }

            @Override
            public void onFailure(Throwable t) {

                editor.putBoolean("isSendRegId",true);
                editor.commit();
            }
        };
        call.enqueue(callback);
    }
}
