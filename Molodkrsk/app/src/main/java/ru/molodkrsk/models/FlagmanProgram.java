package ru.molodkrsk.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Дмитрий on 28.09.2015.
 */
public class FlagmanProgram {

    @SerializedName("id")
    private int id;

    @SerializedName("title")
    private String title;

    @SerializedName("txt")
    private String txt;

    @SerializedName("img")
    private String img;

    public FlagmanProgram(){}

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getTxt() {
        return txt;
    }

    public String getImg() {
        return img;
    }
}
