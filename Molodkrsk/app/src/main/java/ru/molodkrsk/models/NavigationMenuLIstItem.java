package ru.molodkrsk.models;

/**
 * Created by Дмитрий on 14.09.2015.
 */
public class NavigationMenuLIstItem {

    public int icon;

    public String name;

    public NavigationMenuLIstItem(int icon, String name){
        this.icon = icon;
        this.name = name;
    }
}
