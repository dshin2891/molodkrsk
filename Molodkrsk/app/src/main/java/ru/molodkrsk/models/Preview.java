package ru.molodkrsk.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Дмитрий on 24.09.2015.
 */
public class Preview {

    @SerializedName("id")
    private int id;
    @SerializedName("txt")
    private String txt;
    @SerializedName("datetime")
    private String datetime;
    @SerializedName("img")
    private String img;
    @SerializedName("url")
    private String url;
    @SerializedName("flagman_id")
    private String flagman_id;

    public Preview(){}

    public int getId() {
        return id;
    }

    public String getTxt() {
        return txt;
    }

    public String getDatetime() {
        return datetime;
    }

    public String getImg() {
        return img;
    }

    public String getUrl() {
        return url;
    }

    public String getFlagman_id() {
        return flagman_id;
    }
}
